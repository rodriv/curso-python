from fabric.api import run, cd, prompt, sudo, prefix
from fabric.contrib.console import confirm

VENV_PREFIX = 'source /home/fisa/devel/curso-python/venv/bin/activate'


def saludar():
    print 'hola'


def correr():
    print 'corriendo'


def deploy():
    print 'deployando...'

    if confirm('seguro que quiere deployar?'):
        with cd('/opt/noticias/curso-python/'):
            run('git pull origin master')

            with prefix(VENV_PREFIX):
                run('pip install -r setup/requirements.txt')

            with cd('noticias'):
                with prefix(VENV_PREFIX):
                    run('echo "yes" | python manage.py collectstatic')

            sudo('service supervisor restart')
