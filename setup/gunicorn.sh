#!/bin/bash
cd /opt/noticias/curso-python/noticias/
source /opt/noticias/venv/bin/activate
/opt/noticias/venv/bin/gunicorn noticias.wsgi -t 600 -b 127.0.0.1:8800 -w 17 --user=root --group=root --log-file=/opt/noticias/gunicorn.log 2>>/opt/noticias/gunicorn.log


