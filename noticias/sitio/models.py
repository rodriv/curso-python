from django.db import models


class Noticia(models.Model):
    """Este modelo guarda cada noticia individual"""
    titulo = models.CharField(max_length=50)
    texto = models.CharField(max_length=200)
    fecha = models.DateTimeField()
    archivada = models.BooleanField(default=False)

    def publicar(self):
        """Publica la noticia."""
        pass


class LogEdiciones(models.Model):
    noticia = models.ForeignKey(Noticia)
    fecha = models.DateTimeField(auto_now=True)
    accion = models.TextField()
