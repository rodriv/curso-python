from django import forms
from django.core.exceptions import ValidationError
from sitio.models import Noticia


class FormNoticia(forms.ModelForm):
    class Meta:
        model = Noticia
        fields = ['titulo', 'texto', 'fecha']

    def clean_texto(self):
        texto = self.cleaned_data['texto']

        if len(texto.split()) < 4:
            raise ValidationError('Tiene que tener mas de 3 palabras')

        return texto
