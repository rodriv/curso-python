from sitio.models import LogEdiciones


def publicar_noticia(noticia):
    noticia.save()
    log = LogEdiciones.objects.create(noticia=noticia, accion='Creada')
    # send_mail('Se creo una noticia', 'Bla bla bla bla.', 'from@example.com', ['to@example.com'], fail_silently=False)
