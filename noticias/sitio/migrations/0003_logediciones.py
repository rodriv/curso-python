# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitio', '0002_remove_noticia_likes'),
    ]

    operations = [
        migrations.CreateModel(
            name='LogEdiciones',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha', models.DateTimeField(auto_now=True)),
                ('accion', models.TextField()),
                ('noticia', models.ForeignKey(to='sitio.Noticia')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
