from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from sitio.forms import FormNoticia
from sitio.api import publicar_noticia


def home(request):
    return render(request, 'home.html', {})


def crear_noticia(request):
    if request.method == 'POST':
        form = FormNoticia(request.POST)
        if form.is_valid():
            noticia = form.save(commit=False)
            publicar_noticia(noticia)
            return HttpResponseRedirect(reverse('home'))
    else:
        form = FormNoticia()

    return render(request, 'crear_noticia.html', {'form': form})
