from django.contrib import admin
from sitio.models import Noticia, LogEdiciones

# Register your models here.
admin.site.register(Noticia)
admin.site.register(LogEdiciones)
