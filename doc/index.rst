.. noticias documentation master file, created by
   sphinx-quickstart on Thu Mar  5 19:23:52 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to noticias's documentation!
====================================

Contents:

* :doc:`servir_django`
* :doc:`modelos_noticias`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

